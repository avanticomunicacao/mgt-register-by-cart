<?php
declare(strict_types=1);

namespace Avanti\RegisterByCart\Plugin\Magento\Customer\Controller\Account;

use Magento\Customer\Controller\Account\CreatePost as CreatePostCore;
use Magento\Framework\UrlInterface;

class CreatePost
{
    /**
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * CreatePost constructor.
     * @param UrlInterface $urlInterface
     */
    public function __construct(UrlInterface $urlInterface)
    {
        $this->urlInterface = $urlInterface;
    }

    /**
     * @param CreatePostCore $subject
     * @param $result
     * @return mixed
     */
    public function afterExecute(CreatePostCore $subject, $result)
    {
        if(isset($_SESSION['register_by_cart'])) {
            $result->setUrl($this->urlInterface->getUrl('checkout', ['_secure' => true]));
            unset($_SESSION['register_by_cart']);
            unset($_SESSION['already_pass']);
        }
        return $result;
    }
}
