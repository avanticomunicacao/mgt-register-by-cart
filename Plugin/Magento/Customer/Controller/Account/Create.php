<?php
declare(strict_types=1);

namespace Avanti\RegisterByCart\Plugin\Magento\Customer\Controller\Account;

use Magento\Customer\Controller\Account\Create as CreateCore;
use Magento\Store\Model\StoreManagerInterface as StoreInterface;

class Create
{
    /**
     * @var StoreInterface
     */
    protected $storeManager;

    /**
     * Create constructor.
     * @param StoreInterface $storeManager
     */
    public function __construct(StoreInterface $storeManager)
    {
        $this->storeManager = $storeManager;
    }

    /**
     * @param CreateCore $subject
     * @param $result
     * @return mixed
     */
    public function afterExecute(CreateCore $subject, $result)
    {
        $referUrl =  $_SERVER['HTTP_REFERER'] ?? null;
        if ($referUrl) {
            $baseUrl = $this->storeManager->getStore()->getBaseUrl();
            $cartUrl = $baseUrl . 'checkout/cart/';

            if ($referUrl == $cartUrl && !isset($_SESSION['already_pass'])) {
                $_SESSION['register_by_cart'] = true;
                $_SESSION['already_pass'] = true;
            }
        }
        return $result;
    }
}
